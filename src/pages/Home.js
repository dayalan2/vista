import React, { useState, useEffect } from 'react';
import ApartmentList from '../components/ApartmentList';
import Header from '../components/Header';
import { fetchApartments } from '../services/ApartmentService';
import { addToFavorites, removeFromFavorites, fetchUserFavorites } from '../services/FavoritesService';
import { auth } from '../firebase'; 

const Home = () => {
    const [apartments, setApartments] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [userId, setUserId] = useState('0'); 
    const [filterOptions, setFilterOptions] = useState({
        minPrice: '100',
        maxPrice: '20000',
        beds: '1',
        baths: '1'
    });

    const loadApartments = async () => {
        console.log("fetching");
        const data = await fetchApartments();
        setApartments(data);
    };

    useEffect(() => {
        loadApartments();
        
        const loadFavorites = async () => {
            console.log("Fav", userId);
            const userFavorites = await fetchUserFavorites(userId); // Add logic to fetch user ID
            setFavorites(userFavorites);
        };

        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserId(auth.currentUser.uid); 
                console.log(auth.currentUser.uid);
                loadFavorites();
            } 
        })
    }, [userId]);

    const handleFilterChange = (event) => {
        const { name, value } = event.target;

        setFilterOptions({
            ...filterOptions,
            [name]: value
        });
    };

    const createSelectOptions = (options) => {
        return options.map((option, index) => (
            <option key={index} value={option}>
                {option}
            </option>
        ));
    };

    const toggleFavorite = async (apartmentId) => {
        apartmentId = parseInt(apartmentId)
        if (favorites.includes(apartmentId)) {
            await removeFromFavorites(userId, apartmentId);
            setFavorites(favorites.filter(id => id !== apartmentId));
        } else {
            await addToFavorites(userId, apartmentId);
            setFavorites([...favorites, apartmentId]);
        }
    };

    console.log("user apart:", apartments);
    console.log("user favorites:", favorites);

    // Apply filters to the apartments
    const filteredApartments = apartments.filter((apartment) => {
        const { minPrice, maxPrice, beds, baths } = filterOptions;
    
        return (
            (minPrice === '' || apartment.units.some((unit) => unit.rent >= parseInt(minPrice))) &&
            (maxPrice === '' || apartment.units.some((unit) => unit.rent <= parseInt(maxPrice))) &&
            apartment.units.some((unit) => unit.beds === parseInt(beds) && unit.baths === parseInt(baths))
        );
    });

    return (
        <div className="home">
            <Header/>
    
            <div className="container mx-auto my-8">
                <h2 className="text-xl font-semibold mb-4">Search and Filters</h2>
                <div className="flex space-x-4 items-end">
                    <div>
                        <label className="block font-semibold mb-2">Price Range:</label>
                        <div className="flex space-x-2">
                            <input
                                type="number"
                                name="minPrice"
                                value={filterOptions.minPrice}
                                onChange={handleFilterChange}
                                placeholder="Min Price"
                                className="border rounded-lg px-3 py-1"
                            />
                            <input
                                type="number"
                                name="maxPrice"
                                value={filterOptions.maxPrice}
                                onChange={handleFilterChange}
                                placeholder="Max Price"
                                className="border rounded-lg px-3 py-1"
                            />
                        </div>
                    </div>
                    <div>
                        <label className="block font-semibold mb-2">Beds:</label>
                        <select
                            name="beds"
                            value={filterOptions.beds}
                            onChange={handleFilterChange}
                            className="border rounded-lg px-3 py-1"
                        >
                            {createSelectOptions(['1', '2', '3', '4', '5'])}
                        </select>
                    </div>
                    <div>
                        <label className="block font-semibold mb-2">Baths:</label>
                        <select
                            name="baths"
                            value={filterOptions.baths}
                            onChange={handleFilterChange}
                            className="border rounded-lg px-3 py-1"
                        >
                            {createSelectOptions(['1', '2', '3', '4', '5'])}
                        </select>
                    </div>
                </div>
            </div>
    
            <div className="container mx-auto">
                <ApartmentList 
                    apartments={filteredApartments} 
                    favorites={favorites} 
                    toggleFavorite={toggleFavorite} 
                />
            </div>
        </div>
    );
    
};

export default Home;
