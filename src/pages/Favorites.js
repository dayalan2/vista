import React, { useState, useEffect } from 'react';
import ApartmentList from '../components/ApartmentList';
import Header from '../components/Header';
import { fetchApartmentDetails } from '../services/ApartmentService';
import { addToFavorites, removeFromFavorites, fetchUserFavorites } from '../services/FavoritesService';
import { auth } from '../firebase'; 

const Favorites = () => {
    const [favoriteIds, setFavoriteIds] = useState([]);
    const [favoriteApartments, setFavoriteApartments] = useState([]);
    const [userId, setUserId] = useState('0'); 

    useEffect(() => {
        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserId(auth.currentUser.uid); 
                console.log(auth.currentUser.uid);
            } 
        })
        const loadFavorites = async () => {
            const userFavorites = await fetchUserFavorites(userId); // Add logic to get userID
            setFavoriteIds(userFavorites);
            
            const apartments = await Promise.all(
                userFavorites.map(id => fetchApartmentDetails(id))
            );
            setFavoriteApartments(apartments);
        };

        loadFavorites();
    }, [userId]);

    const toggleFavorite = async (apartmentId) => {
        if (favoriteIds.includes(apartmentId)) {
            await removeFromFavorites(userId, apartmentId);
            setFavoriteIds(favoriteIds.filter(id => id !== apartmentId));
        } else {
            await addToFavorites(userId, apartmentId);
            setFavoriteIds([...favoriteIds, apartmentId]);
        }
    };


    return (
        <div className='Favorites'>
            <Header/>
            <div className="container mx-auto my-8">
                <h1 className="text-2xl font-semibold mb-4">My Favorites</h1>
                <ApartmentList 
                    apartments={favoriteApartments} 
                    favorites={favoriteIds} 
                    toggleFavorite={toggleFavorite} 
                />
            </div>
        </div>
    );
};

export default Favorites;
