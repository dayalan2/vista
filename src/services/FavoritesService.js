import { firestore } from '../firebase'; // Adjust the import path as needed
import { collection, doc, getDoc, setDoc, updateDoc, arrayUnion, arrayRemove } from 'firebase/firestore';

export const addToFavorites = async (userId, apartmentId) => {
    // Firebase logic to add to favorites
    const userRef = collection(firestore, 'users'); 
    const docRef = doc(userRef, userId.toString()); 
    // const docRef = doc(userRef, '1'); // test
    const docSnap = await getDoc(docRef); 
    if (docSnap.exists()) {
        console.log("Editing user"); 
        await updateDoc(docRef, {
            favorites: arrayUnion(parseInt(apartmentId)) 
        }); 
    } else {
        console.log("Adding user"); 
        await setDoc(docRef, {
            favorites: [parseInt(apartmentId)] 
        }); 
    }   
};

export const removeFromFavorites = async (userId, apartmentId) => {
    // Firebase logic to remove from favorites
    const userRef = collection(firestore, 'users');
    const docRef = doc(userRef, userId.toString());
    const docSnap = await getDoc(docRef); 
    if (docSnap.exists()) {
        console.log("Deleting favorite"); 
        await updateDoc(docRef, {
            favorites: arrayRemove(parseInt(apartmentId)) 
        }); 
    }
};

export const fetchUserFavorites = async (userId) => {
    // Firebase logic to fetch user favorites
    const userRef = collection(firestore, 'users'); 
    const docRef = doc(userRef, userId.toString()); 
    const docSnap = await getDoc(docRef); 
    return (docSnap.exists()) ? docSnap.data().favorites : []; 
};
