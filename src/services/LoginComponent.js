// LoginComponent.js
import React, { useState } from 'react';
import firebase from '../firebase'; // firebaseConfig 

const LoginComponent = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async () => {
        try {
            const userCredential = await firebase.auth().signInWithEmailAndPassword(email, password);
            // Handle the signed-in user
            console.log(userCredential.user);
        } catch (error) {
            // Handle Errors here.
            console.error(error.message);
        }
    };

    return (
        <div>
            <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            <button onClick={handleLogin}>Login</button>
        </div>
    );
};

export default LoginComponent;
