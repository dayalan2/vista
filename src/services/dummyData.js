const apartments = [
    {
        "id": "1",
        "title": "Modern Apartment Near Campus Center",
        "description": "Ideal for students, this apartment is close to campus facilities and student nightlife.",
        "image": "https://images.unsplash.com/photo-1570129477492-45c003edd2be",
        "location": "Near Campus Center",
        "furnished": true,
        "utilitiesIncluded": true,
        "petFriendly": true,
        "amenities": ["Gym", "Pool", "High-Speed Internet", "Study Lounge", "Bike Storage"],
        "units": [
            {
                "beds": 1,
                "baths": 1,
                "rent": 1000
            },
            {
                "beds": 2,
                "baths": 2,
                "rent": 1200
            },
            {
                "beds": 3,
                "baths": 2,
                "rent": 1400
            }
        ],
        "rating": 4.1
    },
    {
        "id": "2",
        "title": "Cozy Studio Adjacent to Campus",
        "description": "Compact and efficient, perfect for students who enjoy a quiet study environment.",
        "image": "https://images.unsplash.com/photo-1502672260266-1c1ef2d93688?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1650&q=80",
        "location": "Adjacent to Campus",
        "furnished": false,
        "utilitiesIncluded": false,
        "petFriendly": false,
        "amenities": ["Laundry Room", "Campus Shuttle Service"],
        "units": [
            {
                "beds": 0,
                "baths": 1,
                "rent": 700
            },
            {
                "beds": 1,
                "baths": 1,
                "rent": 750
            }
        ],
        "rating": 4.5
    },
    {
        "id": "3",
        "title": "Campus Seaside Luxury Condo",
        "description": "Luxury living with stunning ocean views, perfect for students who want a premium living experience.",
        "image": "https://images.unsplash.com/photo-1600585154340-be6161a56a0c",
        "location": "Campus Seaside Residence",
        "furnished": true,
        "utilitiesIncluded": true,
        "petFriendly": true,
        "amenities": ["Gym", "Pool", "Sauna", "24/7 Security", "Study Rooms", "Campus Security"],
        "units": [
            {
                "beds": 1,
                "baths": 1,
                "rent": 1400
            },
            {
                "beds": 2,
                "baths": 2,
                "rent": 2200
            },
            {
                "beds": 3,
                "baths": 3,
                "rent": 1500
            }
        ],
        "rating": 3.9
    },
    {
        "id": "4",
        "title": "Loft Near Campus Historic Quarter",
        "description": "A blend of modern and traditional, close to historic campus buildings.",
        "image": "https://images.unsplash.com/photo-1574362848149-11496d93a7c7?q=80&w=2884&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
        "location": "Near Campus Historic Quarter",
        "furnished": false,
        "utilitiesIncluded": false,
        "petFriendly": false,
        "amenities": ["Rooftop Terrace", "Campus Wi-Fi Access"],
        "units": [
            {
                "beds": 1,
                "baths": 1,
                "rent": 1300
            },
            {
                "beds": 2,
                "baths": 1,
                "rent": 1500
            }
        ],
        "rating": 3.2
    },
    {
        "id": "5",
        "title": "University Edge Economical Apartment",
        "description": "Economical option, great for students and within walking distance to university classes.",
        "image": "https://images.unsplash.com/photo-1586023492125-27b2c045efd7",
        "location": "University Edge Apartments",
        "furnished": true,
        "utilitiesIncluded": true,
        "petFriendly": true,
        "amenities": ["Bike Storage", "Community Lounge", "On-Site Tutoring Services"],
        "units": [
            {
                "beds": 1,
                "baths": 1,
                "rent": 800
            },
            {
                "beds": 2,
                "baths": 1,
                "rent": 950
            }
        ],
        "rating": 4.7,
    }
];

// const apartments = [
//     {
//     "id": "910ywt6",
//     "name": "Skyline Tower",
//     "bedRange": "2 - 3 Beds",
//     "rentRange": "$2,250 - 3,600",
//     "address": {
//         "country": "US",
//         "lineOne": "519 E Green St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1098378,
//         "longitude": -88.2307607,
//         "fullAddress": "519 E Green St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/T-voffcMHipGTkbflO1J2J1TK1IOtjwRyxUJBfpDARM/102/image.jpg"
//     },
//     {
//     "id": "jcvk767",
//     "name": "Maywood Apartments",
//     "bedRange": "2 - 4 Beds",
//     "rentRange": "$735 - 980",
//     "address": {
//         "country": "US",
//         "lineOne": "51 E John St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1086259,
//         "longitude": -88.2398745,
//         "fullAddress": "51 E John St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/EFJOEYomIt-3ywjuy8QzjyCO7tnDsMvlZPVDYUTlpmU/102/image.jpg"
//     },
//     {
//     "id": "hm8yzs0",
//     "name": "1107 S 2nd St",
//     "bedRange": "Studio - 4 Beds",
//     "rentRange": "$1,150 - 3,300",
//     "address": {
//         "country": "US",
//         "lineOne": "1107 S 2nd St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1045871,
//         "longitude": -88.2378535,
//         "fullAddress": "1107 S 2nd St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/Oa4KCC7ESylG9W8sfB9hh1H9fsG4bHMdRgHeBPrB5ZM/102/image.jpg"
//     },
//     {
//     "id": "bct4hpc",
//     "name": "Legacy202 - Luxury Student Housing",
//     "bedRange": "1 - 4 Beds",
//     "rentRange": "$1,025 - 2,050",
//     "address": {
//         "country": "US",
//         "lineOne": "202 E Daniel St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1082557,
//         "longitude": -88.236631,
//         "fullAddress": "202 E Daniel St, Champaign, IL 61820"
//     }, 
//     "image" : "https://images1.apartments.com/i2/VgB-eXXmGx0WxHzu0RLDnV5YZiAZgueF1Wtvvfx-Yjo/102/image.jpg"
//     },
//     {
//     "id": "wxelksh",
//     "name": "Capstone Quarters",
//     "bedRange": "2 - 4 Beds",
//     "rentRange": "$445 - 615",
//     "address": {
//         "country": "US",
//         "lineOne": "1901 N Lincoln Ave",
//         "lineTwo": "Urbana, IL 61801",
//         "city": "Urbana",
//         "state": "IL",
//         "postalCode": "61801",
//         "latitude": 40.1297372,
//         "longitude": -88.2212061,
//         "fullAddress": "1901 N Lincoln Ave, Urbana, IL 61801"
//     }, 
//     "image": "https://images1.apartments.com/i2/jvLRkH1L1XC4jO5qlpRV1CbVzaazP2__pMIjqyKV9Jc/102/image.jpg"
//     },
//     {
//     "id": "7yrs9vx",
//     "name": "112 Green",
//     "bedRange": "2 - 4 Beds",
//     "rentRange": "$1,800 - 3,600",
//     "address": {
//         "country": "US",
//         "lineOne": "112 E Green St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1104298,
//         "longitude": -88.2375389,
//         "fullAddress": "112 E Green St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/toBb8lnziy5qQ_GMsrfz0qSm1YbttepLRTccUSq3fBU/102/image.jpg"
//     },
//     {
//     "id": "wlk1bpk",
//     "name": "Smile Student Living - 615 S Wright",
//     "bedRange": "2 - 3 Beds",
//     "rentRange": "$1,800 - 2,650",
//     "address": {
//         "country": "US",
//         "lineOne": "615 S Wright St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.11103,
//         "longitude": -88.2292826,
//         "fullAddress": "615 S Wright St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/GlIUtQBugdiDb1Qeyjgn6anX8rvOzBZzYGBUOVYNNDA/102/image.jpg"
//     },
//     {
//     "id": "qdf14jw",
//     "name": "Pacifica on Green",
//     "bedRange": "1 - 4 Beds",
//     "rentRange": "$875 - 2,070",
//     "address": {
//         "country": "US",
//         "lineOne": "28 E Green St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1105861,
//         "longitude": -88.2414433,
//         "fullAddress": "28 E Green St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/MRZVZqtVZopWd1p7am2EYyhtRs1Icdp0Rpm0jCwVJUc/102/image.jpg"
//     },
//     {
//     "id": "7jkpfzy",
//     "name": "Lancaster Apartments",
//     "bedRange": "3 - 4 Beds",
//     "rentRange": "$760 - 950",
//     "address": {
//         "country": "US",
//         "lineOne": "112 E Chalmers St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1067176,
//         "longitude": -88.2371367,
//         "fullAddress": "112 E Chalmers St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/UE-XV6eWj16VPkeIT4JLroY0j3V2hqWsheT9E34CWB0/102/image.jpg"
//     },
//     {
//     "id": "7dsryqz",
//     "name": "Champaign Park",
//     "bedRange": "1 - 2 Beds",
//     "rentRange": "$725 - 950",
//     "address": {
//         "country": "US",
//         "lineOne": "2106 W White St",
//         "lineTwo": "Champaign, IL 61821",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61821",
//         "latitude": 40.1143466,
//         "longitude": -88.2853796,
//         "fullAddress": "2106 W White St, Champaign, IL 61821"
//     }, 
//     "image": "https://images1.apartments.com/i2/sb4j7YVBTIzZN8tl60ndKEHPhfolJgE7pUDaI-xHFE4/102/image.jpg"
//     },
//     {
//     "id": "dlj34rj",
//     "name": "Parkside- Pets Allowed",
//     "bedRange": "2 Beds",
//     "rentRange": "$899 - 1,075",
//     "address": {
//         "country": "US",
//         "lineOne": "1205 E Florida Ave",
//         "lineTwo": "Urbana, IL 61801",
//         "city": "Urbana",
//         "state": "IL",
//         "postalCode": "61801",
//         "latitude": 40.0980917,
//         "longitude": -88.1946083,
//         "fullAddress": "1205 E Florida Ave, Urbana, IL 61801"
//     }, 
//     "image": "https://images1.apartments.com/i2/opxWjLbWUZ8PMmUqhjdhB4aVGsXlyw64HjAqOfEGNh4/102/image.jpg"
//     },
//     {
//     "id": "5y9mhqq",
//     "name": "Eastland Apartments, LLC",
//     "bedRange": "Studio - 2 Beds",
//     "rentRange": "$859 - 2,125",
//     "address": {
//         "country": "US",
//         "lineOne": "1905 N Cunningham Ave",
//         "lineTwo": "Urbana, IL 61802",
//         "city": "Urbana",
//         "state": "IL",
//         "postalCode": "61802",
//         "latitude": 40.1309766,
//         "longitude": -88.1981201,
//         "fullAddress": "1905 N Cunningham Ave, Urbana, IL 61802"
//     }, 
//     "image": "https://images1.apartments.com/i2/S1mVO_YW8n9iudnNjRdZXL928pocZ_Xq-0gD6yPUFRk/102/image.jpg"
//     },
//     {
//     "id": "g2zhk4y",
//     "name": "Town and Country",
//     "bedRange": "1 - 2 Beds",
//     "rentRange": "$665 - 1,165",
//     "address": {
//         "country": "US",
//         "lineOne": "1032 E Kerr Ave",
//         "lineTwo": "Urbana, IL 61801",
//         "city": "Urbana",
//         "state": "IL",
//         "postalCode": "61801",
//         "latitude": 40.122221,
//         "longitude": -88.196652,
//         "fullAddress": "1032 E Kerr Ave, Urbana, IL 61801"
//     }, 
//     "image": "https://images1.apartments.com/i2/HB2hydUDuBX8A3IGe1kD9QVtnQW5ZExNVR7rRKuEDKg/102/image.jpg"
//     },
//     {
//     "id": "vn1k7tf",
//     "name": "Park Place Tower",
//     "bedRange": "1 - 4 Beds",
//     "rentRange": "$1,375 - 3,500",
//     "address": {
//         "country": "US",
//         "lineOne": "202 E Green St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1107449,
//         "longitude": -88.2366578,
//         "fullAddress": "202 E Green St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/R7RGwAkztXldSvgtu7Zblo3DVaqCTQooGec2AQmCbxA/102/image.jpg"
//     },
//     {
//     "id": "04et261",
//     "name": "The Landing at Legends 55+ Luxury Apartments",
//     "bedRange": "1 - 2 Beds",
//     "rentRange": "$1,699 - 2,149",
//     "address": {
//         "country": "US",
//         "lineOne": "4503 Legends Dr",
//         "lineTwo": "Champaign, IL 61822",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61822",
//         "latitude": 40.0708073,
//         "longitude": -88.3176274,
//         "fullAddress": "4503 Legends Dr, Champaign, IL 61822"
//     }, 
//     "image": "https://images1.apartments.com/i2/hUj567IZRLGsYG6Mx16QLhD6nVL5F6Y4A9L-3Otkaq0/102/image.jpg"
//     },
//     {
//     "id": "7fd6ggy",
//     "name": "Copper View Apartments",
//     "bedRange": "2 - 3 Beds",
//     "rentRange": "$1,199 - 1,299",
//     "address": {
//         "country": "US",
//         "lineOne": "2002 W Bradley Ave",
//         "lineTwo": "Champaign, IL 61821",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61821",
//         "latitude": 40.1283057,
//         "longitude": -88.2813656,
//         "fullAddress": "2002 W Bradley Ave, Champaign, IL 61821"
//     }, 
//     "image": "https://images1.apartments.com/i2/5jzX2jo0-XM5cQPSoV9e8KL_s652m-cJr0ULekUvgx0/102/image.jpg"
//     },
//     {
//     "id": "pf0recs",
//     "name": "410 E. Green St",
//     "bedRange": "Studio - 3 Beds",
//     "rentRange": "$1,050 - 3,000",
//     "address": {
//         "country": "US",
//         "lineOne": "410 E Green St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1106259,
//         "longitude": -88.2323232,
//         "fullAddress": "410 E Green St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/n85TCMvHUgZQySFhvU0AeSa2FNG-LOS_nKCK8THXFLY/102/image.jpg"
//     },
//     {
//     "id": "w7dm2dj",
//     "name": "The Quarters",
//     "bedRange": "1 - 2 Beds",
//     "rentRange": "$1,050 - 1,325",
//     "address": {
//         "country": "US",
//         "lineOne": "11 E Columbia Ave",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1209,
//         "longitude": -88.2428,
//         "fullAddress": "11 E Columbia Ave, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/7lF9koFmtJOejtICDf3TD6nPeVaL9k0RYM8UtH7u9WE/102/image.jpg"
//     },
//     {
//     "id": "f4cwczs",
//     "name": "Seven07",
//     "bedRange": "Studio - 4 Beds",
//     "rentRange": "$1,059 - 1,939",
//     "address": {
//         "country": "US",
//         "lineOne": "707 S 4th St",
//         "lineTwo": "Champaign, IL 61820",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61820",
//         "latitude": 40.1096859,
//         "longitude": -88.2339995,
//         "fullAddress": "707 S 4th St, Champaign, IL 61820"
//     }, 
//     "image": "https://images1.apartments.com/i2/yI_vOGHFLSjSR7fuXTtN6vEcPux3OoeLbY-pKtkIda8/102/image.jpg"
//     },
//     {
//     "id": "9111y84",
//     "name": "Baytowne Apartments",
//     "bedRange": "1 - 2 Beds",
//     "rentRange": "$960 - 2,205",
//     "address": {
//         "country": "US",
//         "lineOne": "1000 Baytowne Dr",
//         "lineTwo": "Champaign, IL 61822",
//         "city": "Champaign",
//         "state": "IL",
//         "postalCode": "61822",
//         "latitude": 40.1408233,
//         "longitude": -88.262687,
//         "fullAddress": "1000 Baytowne Dr, Champaign, IL 61822"
//     }, 
//     "image": "https://images1.apartments.com/i2/kjSKbBuD7Wu1XHKilTbpA4hjcJrEiyUTUYVbJfrlW5w/102/image.jpg"
//     }
// ]
export default apartments;


