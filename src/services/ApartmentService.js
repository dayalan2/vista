// ApartmentService.js
// const API_BASE_URL = "https://your-api-url.com"; // Replace with your API's base URL
import axios from 'axios'; 
// const axios = require('axios');

const api_url = "https://my-json-server.typicode.com/justinwookim/apartments-api/apartments"

const processApartmentData = (jsonData) => {
    return jsonData.map(item => ({
        id: item.id,
        title: item.title,
        description: item.description,
        image: item.image, // Ensure this is a URL to an image
        location: item.location,
        furnished: item.furnished,
        utilitiesIncluded: item.utilitiesIncluded,
        petFriendly: item.petFriendly,
        amenities: item.amenities,
        units: item.units, // An array of unit types with their specific details
        rating: item.rating
    }));
};

const fetchApartments = async () => {
    const response = await axios.get(api_url);  
    console.log(response);
    return processApartmentData(response.data); 
};

const fetchApartmentDetails = async (apartmentId) => {
    const response = await axios.get(api_url + "/" +apartmentId); 
    return response.data; 
};

export { fetchApartments, fetchApartmentDetails };
