import React from 'react';
import { Link } from 'react-router-dom';
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';

const ApartmentList = ({ apartments, favorites, toggleFavorite }) => {
    console.log("Apartments: ",apartments);
    console.log("Favorites: ",favorites);
    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 p-4">
            {apartments.map(apartment => (
                <div key={apartment.id} className="relative bg-white rounded-lg shadow-md overflow-hidden hover:shadow-lg transition-shadow">
                    <Link to={`/vista/apartment/${apartment.id}`} className="block">
                        <img src={apartment.image} alt={apartment.title} className="w-full h-60 object-cover" />
                        <div className="p-4">
                        <h2 className="text-xl font-semibold mb-2">{apartment.title}</h2>
                        <p className="text-gray-600 mb-1">{apartment.location}</p>
                        {apartment.units.map(unit => (
                            <p key={unit.beds} className="text-gray-800 font-bold mb-1">
                                {unit.beds} bed, {unit.baths} bath: Rent: ${unit.rent}
                            </p>
                        ))}
                        <div className="flex items-center">
                            <span className="text-black font-bold mr-1">{apartment.rating}</span>
                            <svg className="h-5 w-5 text-black" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.049 2.927c.396-.802 1.47-.802 1.866 0l1.888 3.833c.145.295.422.5.744.555l4.239.617c.867.126 1.215 1.196.587 1.805l-3.067 2.992c-.235.23-.342.573-.286.909l.725 4.233c.151.885-.738 1.561-1.511 1.144l-3.807-2.004c-.307-.162-.676-.162-.983 0l-3.807 2.004c-.773.417-1.662-.259-1.511-1.144l.725-4.233c.056-.336-.051-.678-.286-.909l-3.067-2.992c-.628-.609-.28-1.679.587-1.805l4.239-.617c.322-.055.599-.26.744-.555l1.888-3.833z" />
                            </svg>
                        </div>
                    </div>
                    </Link>
                    <button 
                        onClick={(e) => {
                            e.stopPropagation(); // Prevent link navigation
                            toggleFavorite(parseInt(apartment.id));
                        }} 
                        className="absolute top-0 right-0 m-2 text-2xl text-red-500 bg-white rounded-full p-2 shadow-lg"
                    >
                        {favorites.includes(parseInt(apartment.id)) ? <AiFillHeart /> : <AiOutlineHeart />}
                    </button>
                </div>
            ))}
        </div>
    );
};

export default ApartmentList;
