import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
  onAuthStateChanged
} from 'firebase/auth';
import { auth } from '../firebase';

const Header = () => {
    const [user, setUser] = useState(null); // Start with no user

    useEffect(() => {
        // Listen for auth state changes
        const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
            setUser(currentUser); // Set the user or null
        });

        // Clean up the listener when the component unmounts
        return () => unsubscribe();
    }, []);

    const handleSignIn = async (e) => {
        e.preventDefault(); // Prevent the default form submit behavior
        const provider = new GoogleAuthProvider();
        try {
            await signInWithPopup(auth, provider);
            // The user will be set in the onAuthStateChanged listener
        } catch (error) {
            console.error("Error during sign in:", error);
        }
    };

    const handleSignOut = async (e) => {
        e.preventDefault();
        try {
            await signOut(auth);
            // The user will be set to null in the onAuthStateChanged listener
        } catch (error) {
            console.error("Error during sign out:", error);
        }
    };

    return (
        <header className="bg-blue-500 py-4 px-8 text-white">
            <div className="container mx-auto flex justify-between items-center">
                <Link to="/vista" className="border rounded-lg px-3 py-1 text-2xl font-semibold hover:text-gray-200">
                    Home
                </Link>
                <div className="flex space-x-4">
                    <Link to="/vista/favorites" className="border rounded-lg px-3 py-1 hover:bg-white hover:text-blue-500">
                        Favorites
                    </Link>
                    {user ? (
                        <button onClick={handleSignOut} className="border rounded-lg px-3 py-1 hover:bg-white hover:text-blue-500">Welcome, {user.displayName}!</button>
                    ) : (
                        <button onClick={handleSignIn} className="border rounded-lg px-3 py-1 hover:bg-white hover:text-blue-500">Sign In</button>
                    )}
                    
                </div>
            </div>
        </header>
    );
};

export default Header;
