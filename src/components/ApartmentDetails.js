import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { fetchApartmentDetails } from '../services/ApartmentService';
import Header from './Header';

const ApartmentDetails = () => {
    const [apartment, setApartment] = useState(null);
    const { id } = useParams(); // Get the apartment ID from URL parameters

    useEffect(() => {
        const loadApartmentDetails = async () => {
            const data = await fetchApartmentDetails(id);
            setApartment(data);
        };

        loadApartmentDetails();
    }, [id]);

    if (!apartment) {
        return <div className="min-h-screen flex items-center justify-center">Loading...</div>;
    }

    return (
        <div>
            <Header/>
            <div className="bg-gray-100 min-h-screen p-8">
                <div className="container mx-auto bg-white rounded-lg shadow-md p-6">
                    <h2 className="text-3xl font-semibold mb-4">{apartment.title}</h2>
                    <img src={apartment.image} alt={apartment.title} className="w-full rounded-lg mb-6" />
                    <p className="text-gray-600 mb-6">{apartment.description}</p>

                    {/* Card-like segments */}
                    <div className="bg-gray-200 rounded-lg p-4 mb-6">
                        <h3 className="text-xl font-semibold">Apartment Details</h3>
                        <ul className="list-disc pl-4">
                            <li><span className="font-semibold">Location:</span> {apartment.location}</li>
                            <li><span className="font-semibold">Furnished:</span> {apartment.furnished ? 'Yes' : 'No'}</li>
                            <li><span className="font-semibold">Utilities Included:</span> {apartment.utilitiesIncluded ? 'Yes' : 'No'}</li>
                            <li><span className="font-semibold">Pet Friendly:</span> {apartment.petFriendly ? 'Yes' : 'No'}</li>
                        </ul>
                    </div>

                    <div className="bg-gray-200 rounded-lg p-4 mb-6">
                        <h3 className="text-xl font-semibold">Amenities</h3>
                        <ul className="list-disc pl-4">
                            {apartment.amenities.map((amenity, index) => (
                                <li key={index}>{amenity}</li>
                            ))}
                        </ul>
                    </div>

                    <div className="bg-gray-200 rounded-lg p-4 mb-6">
                        <h3 className="text-xl font-semibold">Units Available</h3>
                        <ul className="list-disc pl-4">
                            {apartment.units.map((unit, index) => (
                                <li key={index}>
                                    {unit.beds} Bedroom(s), {unit.baths} Bathroom(s) - Rent: ${unit.rent}
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="bg-gray-200 rounded-lg p-4">
                        <h3 className="text-xl font-semibold">Rating</h3>
                        <p>{apartment.rating}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ApartmentDetails;
