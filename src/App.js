import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import ApartmentDetails from './components/ApartmentDetails';
import Favorites from './pages/Favorites';

const App = () => {
    return (
        <Router >
            <div className="App">
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/vista" element={<Home />} />
                    <Route path="/vista/apartment/:id" element={<ApartmentDetails />} />
                    <Route path="/vista/favorites" element={<Favorites />} />
                </Routes>
            </div>
        </Router>
    );
};

export default App;
