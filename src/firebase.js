// import { initializeApp } from 'firebase/app';
// import { getFirestore } from 'firebase/firestore';

// const firebaseConfig = {
//   // Your Firebase config
// };

// const app = initializeApp(firebaseConfig);
// const firestore = getFirestore(app);

// export { firestore };


// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from '@firebase/firestore'; 
import { getAuth } from 'firebase/auth'; 
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB9sFpz1Z5gxXlxXDp1SmLOhD-r2JKVQLs",
  authDomain: "vista-5d353.firebaseapp.com",
  projectId: "vista-5d353",
  storageBucket: "vista-5d353.appspot.com",
  messagingSenderId: "1049549856379",
  appId: "1:1049549856379:web:03c57f66a97f619d3a0cde",
  measurementId: "G-FTLZZG9WPS"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
const firestore = getFirestore(app); 
const auth = getAuth(app); 
export { firestore, auth }; 


// const axios = require('axios');

// const options = {
//   method: 'GET',
//   url: 'https://apartments-com1.p.rapidapi.com/properties',
//   params: {
//     location: 'Champaign',
//     // minRent: '100',
//     // minBed: '1',
//     // maxRent: '1000',
//     // page: '2',
//     // maxBed: '3',
//     // maxBath: '3',
//     // minBath: '1',
//     // sort: 'default'
//   },
//   // headers: {
//   //   'X-RapidAPI-Key': 'SIGN-UP-FOR-KEY',
//   //   'X-RapidAPI-Host': 'apartments-com1.p.rapidapi.com'
//   // }
// };

// try {
// 	const response = await axios.request(options);
// 	console.log(response.data);
    
// } catch (error) {
// 	console.error(error);
// }

/* 
  - Do a GET request for apartments in Champaign, IL so that we can add the list of 
    apartments to an array and our database 
  - Setup some endpoints for our favorites so that the user can retrieve their favorited apartments 
  - 
*/ 